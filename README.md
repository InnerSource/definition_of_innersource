# 内源
内源是在组织内使用开源软件开发最佳实践并建立类开源文化。该组织可以在组织内部开源的同时开发专有软件。 内源这个词是Tim O'Reilly在2000年提出的。

# 动机

开源被认为能够交付高质量的软件。[2] 此外，开源世界中的开放式协作可以促进协作，甚至是竞争者之间（例如，ARM和英特尔根据基于绩效的决策开发Linux内核）。

因此，软件开发组织希望从其成果（软件组件和工具）中受益，也希望从开源世界中实践和建立的开发实践中受益。

# 进行开源实践

除了诸如Apache软件基金会，Linux基金会和Eclipse基金会等基金会建立的一些实践之外，内源和开源项目还需要开放式协作、开放式沟通和良好的质量保证等实践。

### 开放式协作
在推行内源的公司，所有员工都必须可以访问所有需要的开发制品（例如，代码、文档、问题跟踪等）。集中软件构建是实施开放式协作的重要工具。
基于开放式协作的原则（平等的、精英领导的、自组织的），通常欢迎愿意为内源项目提供帮助的所有贡献者。 对内源项目的贡献通常是根据其为项目带来的价值来进行评估。 公开讨论决策时，开放式沟通也实现了精英制度。 尽管组织不一定要变成彻底的自组织来适应内源，但是内源允许个人，组织单元和项目团体具有更高程度的自组织。

### 开放式沟通
​开放式的沟通可以让内源项目和软件中的所有成员能够公开参与所有的交流互动。 开放式沟通是公开的（在公司内部）、书面的、有存档且完整的。 目的是允许与内源项目有关或感兴趣的任何个人或团体参与沟通。 开放式沟通是会被存档的，软件的详细文档会被收集起来，使你可以回过头来回顾当时的讨论和决策。


### 通过分离角色保证产品质量

专门的代码审查以及贡献者和提交者（拥有写入权限的集成者、开发者）分离，可以确保开源项目的质量，也可以保证内源项目的质量。

# 收益

除了开源项目的质量收益，还有如下收益：

**开发更有效和高效**

* 更快推向市场
* 降低开发成本

**克服组织单元边界（打破部门墙）**

* 组织单元成本和风险共担
* 跨越组织单元边界合作
* 应用程序级信息互通

**更多成功重用**

* 可以使用单独组件无法提供的能力
* 复用者和提供者之间相互独立
* 组件提供者变轻松

**更好的软件产品**

* 代码质量提升
* 更多创新性开发

**更灵活地利用开发人员**

* 简化开发人员部署
* 无关联的开发人员相互合作

**增强知识管理**

* 社区学习
* 知识开放性和可用性

**员工动力更足**

# 流行

如下的公司已明确实施了内源： 

* HP
* Philips
* Lucent
* Nokia
* IBM
* DTE
* Robert Bosch
* Google
* Microsoft
* SAP
* PayPal[5]
* Capital One[6]
* Amdocs
* Skyscanner
* Comcast
* T-Mobile

# 实施内源的关键因素
内源对于大型软件开发公司是有明确收益的方法。 但是，它可能不适用于所有环境。 可将以下9个因素分为三类，以评估内源适用的程度。

### 产品因素
* 形成社区的种子产品
* 作出不同贡献的多方干系人
* 吸引贡献者和用户的模块化

### 流程和工具因素
* 支持“集市式”开发的实践
* 支持“集市式”质量保证的实践
* 促进协作的工具标准化

### 组织和社区因素
* 支持内部精英文化形成的协作和领导力
* 开放组织的透明
* 支持和鼓励员工参与的管理

# References

1.	O'Reilly, Tim (2000-12-01). "Open Source and OpenGL". oreilly.com. O'Reilly and Associates. Archived from the original on 2015-02-15. Retrieved 2017-02-22. [W]e've also worked with companies on what we call “inner sourcing” — that is, helping them to use open source development techniques within the corporation. 
2.	Kevin Crowston, Kangning Wei, James Howison, Andrea Wiggins (2012), ACM (ed.), "Free/Libre open-source software development: What we know and what we do not know", ACM Computing Surveys (in German), 44 (2): 1–35, doi:10.1145/2089125.2089127 
3.	Capraro, Maximilian; Riehle, Dirk (2016-12-01). "Inner Source Definition, Benefits, and Challenges". ACM Comput. Surv. 49 (4): 67:1–67:36. doi:10.1145/2856821. ISSN 0360-0300. 
4.	Stol, Klaas-Jan; Fitzgerald, Brian (2015-07-01). "Inner Source - Adopting Open Source Development Practices within Organizations: A tutorial" (PDF). IEEE Software. 32 (4): 60–67. doi:10.1109/MS.2014.77. hdl:10344/4443. ISSN 0740-7459. 
5.	Oram, Andy (2015). Getting Started with InnerSource. O’Reilly Media, Inc. ISBN 978-1-491-93758-7. 
6.	Smith, Jared (2016). Using open source methods for internal software projects. O’Reilly Media, Inc. 
7.	Stol, K. J.; Avgeriou, P.; Babar, M. A.; Lucas, Y.; Fitzgerald, B. (2014). "Key factors for adopting inner source". ACM Transactions on Software Engineering and Methodology. 23 (2): 1. doi:10.1145/2533685. hdl:10344/3897. 
