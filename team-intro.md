内源（InnerSource）是使用开源软件开发最佳实践并在组织内建立类似开源的文化。 该组织可能仍会开发专有软件，但会在内部开放其开发。 这个术语是[蒂姆·奥莱利（Tim O'Reilly）](https://en.wikipedia.org/wiki/Tim_O%27Reilly)在2000年创造的。

### 采用内源的好处

除了开源项目的质量收益，还有如下收益：

**开发更有效和高效**

* 更快推向市场
* 降低开发成本

**克服组织单元边界（打破部门墙）**

* 组织单元成本和风险共担
* 跨越组织单元边界合作
* 应用程序级信息互通

**更多成功重用**

* 可以使用单独组件无法提供的能力
* 复用者和提供者之间相互独立
* 组件提供者变轻松

**更好的软件产品**

* 代码质量提升
* 更多创新性开发

**更灵活地利用开发人员**

* 简化开发人员部署
* 无关联的开发人员相互合作

**增强知识管理**

* 社区学习
* 知识开放性和可用性

**员工动力更足**

### 内源图书 （PDF下载）

<a href="https://gitee.com/InnerSource/definition_of_innersource/raw/master/getting-started-with-innersource.pdf"><img src='https://images.gitee.com/uploads/images/2020/0219/084340_c14103b7_36.gif' width=200/></a> <a href="https://gitee.com/InnerSource/definition_of_innersource/raw/master/AdoptingInnerSource.pdf"><img src='https://images.gitee.com/uploads/images/2020/0219/105503_0f0fb261_36.png' title='Adopting InnerSource' width=200/></a>

PPT分享（百度）：[内部开源之 WHAT、WHY、HOW](https://gitee.com/OSCYuanChuangHui/2018_lecturer_ppt/raw/master/2019.12.15%20%E6%B7%B1%E5%9C%B3%E5%B9%B4%E7%BB%88%E7%9B%9B%E5%85%B8/%E5%BC%80%E6%BA%90%E6%B2%BB%E7%90%86/3.%20%E5%86%85%E9%83%A8%E5%BC%80%E6%BA%90%E7%9A%84%20WHAT%20:%20WHY%20:%20HOW%E2%80%94%E2%80%94%E8%B0%AD%E4%B8%AD%E6%84%8F.pdf) 

### Gitee 如何支持企业内源

[Gitee 企业版](https://gitee.com/enterprises) 为企业的内部开源治理提供了直接的支持。在 Gitee 企业版中你可以直接在”内源“频道中集中管理企业的所有外部和内部的开源项目。内源频道聚合企业开源的所有信息，包括社区的参与贡献、评价等等。企业技术负责人可以清楚了解企业在开源领域所取得的成就。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0227/163113_bdaf2b83_1305863.jpeg "12.jpg")

想了解更多关于 Gitee 对企业内源治理的支持，请阅读 [Gitee 企业内源最佳实践](https://gitee.com/InnerSource/gitee-innersource-best-practices) 


立即前往 [开通 Gitee 企业版](https://gitee.com/enterprises/new?from=innersource) ，体验 Gitee 的企业内源治理能力。

### 其他内源文章

* [Inner Source in Platform-Based Product Engineering](https://gitee.com/InnerSource/definition_of_innersource/raw/master/InnerSourceinPlatform-BasedProductEngineeringIEEETSE.pdf)
* [Inner Source—Adopting Open Source Development Practices in Organizations](https://www.infoq.com/articles/inner-source-open-source-development-practices/)
* [InnerSource with GitLab](https://about.gitlab.com/solutions/innersource/)
* [Why You Need to Care About Inner Sourcing](https://www.laserfiche.com/ecmblog/why-you-need-to-care-about-inner-sourcing/)
* [InnerSourcing: the development model of the future?](https://blog.bitergia.com/2016/07/26/innersourcing-the-development-model-of-the-future/)
* [Innersource: How to leverage open source in the enterprise](https://opensource.com/article/17/9/innersource)
* [Microsoft is going all-in on 'Inner Source'](https://www.zdnet.com/article/microsoft-is-going-all-in-on-inner-source/)
* [Salesforce Engineering: Inner Sourcing: What’s this?](https://engineering.salesforce.com/inner-sourcing-whats-this-ef2220ae59ec)
* [BOSCH: Know how Open and Inner Source](https://www.bosch.com/research/know-how/open-and-inner-source/)
* [Adobe: Open Development vs. Inner Source](https://medium.com/adobetech/open-development-vs-inner-source-fdccf573a242)
* [ACM: Inner Source Definition, Benefits, and Challenges](https://dl.acm.org/doi/10.1145/2856821)
* [Why open source may trump inner source for internal collaboration](https://www.techrepublic.com/article/why-open-source-may-trump-inner-source-for-internal-collaboration/)
* [Innersource: A Guide to the What, Why, and How](https://www.jonobacon.com/2017/06/25/innersource-guide/)
* [IEEE: Inner Source--Adopting Open Source Development Practices in Organizations: A Tutorial](https://ieeexplore.ieee.org/document/6809709)