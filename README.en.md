InnerSource is the use of open source software development best practices and the establishment of an open source-like culture within organizations. The organization may still develop proprietary software, but internally opens up its development. The term was coined by Tim O'Reilly in 2000.

# Motivation
Open source is recognized to be capable of delivering high quality software.[2] Furthermore, the open collaboration in open source enables collaboration even between competitors (e.g. ARM and Intel working on Linux kernel on merit-based decisions).

Consequently, software developing organizations want to benefit from its outcomes (the software components and tools), but also from the development practices exercised and established in the open source world.

Used open source practices
Besides several practices established in foundations such as Apache Software Foundation, Linux Foundation, and Eclipse Foundation, inner source and open source projects require open collaboration, open communication, and a proper quality assurance.

## Open collaboration
All required development artifacts (e.g. code, documentation, issue tracker, etc.) have to be accessible for all employees of a company leveraging inner source. Central software forges are an essential tool for implementing open collaboration.

Based on the principles of open collaboration (egalitarian, meritocratic, and self-organizing) every contributor who is willing to help an InnerSource project is typically welcome. Contributions to InnerSource projects are typically judged meritocratically based on the value they bring to the project. Meritocracy can also be enabled by open communication as decisions are discussed publicly. Although an organization does not necessarily become completely self-organizing to adopt inner source, inner source allows individuals, organizational units, and project communities a higher degree of self-organization.

## Open communication
InnerSource projects and programs rely on open communication to make all communication openly accessible for all employees. Open communication is communication that is public (within the company), written, archived, and complete. The goal is to allow any individual or party that has stake or interest in an InnerSource project to participate in the communication. As open communication discussions are archived, a detailed documentation of the software is passively gathered that allows one to go back and revisit historic discussions and decisions.

Quality assurance through separation of contribution from integration
A dedicated code review and the separation of contributors and committers (integrators, developers with write access) assures the quality of an open source project, and, therefore, also for an inner source project.

# Benefits
Beyond the quality attributes of open source software the following benefits are reported:

**More efficient and effective development**

* Faster Time-to-Market
* Reduced development costs

**Overcoming organizational unit boundaries**

* Cost and risk sharing among organizational units
* Collaboration across organizational unit boundaries
* Program-wide information exchange

**More successful reuse**

* Use of competences missing at component providers
* Independence between reusers and providers
* Relief of component providers

**Better software product**

* increased code quality
* More innovative development

**More flexible utilization of developers**

* Simplified developer deployment
* Collaboration of detached developers

**Enhanced knowledge management**

* Community-based learning
* Openness and availability of knowledge

**Higher employee motivation**

#Prevalence
Among others the following companies are known for adopting InnerSource:[3]

* HP
* Philips
* Lucent
* Nokia
* IBM
* DTE
* Robert Bosch
* Google
* Microsoft
* SAP
* PayPal
* Capital One
* Amdocs
* Skyscanner
* Comcast
* T-Mobile

# Key factors for adopting InnerSource

InnerSource can be a promising approach for large organizations that develop software. However, it may not be appropriate in all settings. The following nine factors, grouped in three categories, can be consulted to gauge the extent to which InnerSource may be appropriate.[7]

## Product factors

* Seed product to attract a community
* Multiple stakeholders for a variety of contributions
* Modularity to attract contributors and users

## Process and Tools factors

* Practices that support "Bazaar-style" development
* Practices that support "Bazaar-style" quality assurance
* Standardization of tools to facilitate collaboration

## Organization and Community factors

* Coordination and leadership to support the emergence of an internal meritocracy
* Transparency to open up the organization
* Management support and motivation to involve people

# References
 
 1. O'Reilly, Tim (2000-12-01). "Open Source and OpenGL". oreilly.com. O'Reilly and Associates. Archived from the original on 2015-02-15. Retrieved 2017-02-22. [W]e've also worked with companies on what we call “inner sourcing” — that is, helping them to use open source development techniques within the corporation.
 2. Kevin Crowston, Kangning Wei, James Howison, Andrea Wiggins (2012), ACM (ed.), "Free/Libre open-source software development: What we know and what we do not know", ACM Computing Surveys (in German), 44 (2): 1–35, doi:10.1145/2089125.2089127
 3. Capraro, Maximilian; Riehle, Dirk (2016-12-01). "Inner Source Definition, Benefits, and Challenges". ACM Comput. Surv. 49 (4): 67:1–67:36. doi:10.1145/2856821. ISSN 0360-0300.
 4. Stol, Klaas-Jan; Fitzgerald, Brian (2015-07-01). "Inner Source - Adopting Open Source Development Practices within Organizations: A tutorial" (PDF). IEEE Software. 32 (4): 60–67. doi:10.1109/MS.2014.77. hdl:10344/4443. ISSN 0740-7459.
 5. Oram, Andy (2015). Getting Started with InnerSource. O’Reilly Media, Inc. ISBN 978-1-491-93758-7.
 6. Smith, Jared (2016). Using open source methods for internal software projects. O’Reilly Media, Inc.
 7. Stol, K. J.; Avgeriou, P.; Babar, M. A.; Lucas, Y.; Fitzgerald, B. (2014). "Key factors for adopting inner source". ACM Transactions on Software Engineering and Methodology. 23 (2): 1. doi:10.1145/2533685. hdl:10344/3897.